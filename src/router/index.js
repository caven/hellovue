import Vue from 'vue'
import VueRouter from 'vue-router'
import Header from '@/components/Header'
import Footer from '@/components/Footer'
import Main from '@/components/Main'
import About from '@/components/About'
import Gallery from '@/components/Gallery'
import Events from '@/components/Events'
import Projects from '@/components/Projects'
import Chat from '@/components/Chat'
import Contact from '@/components/Contact'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    components: {
      default: Main,
      header: Header,
      footer: Footer
    }
  },
  { path: '/about', component: About },
  { path: '/gallery', component: Gallery },
  { path: '/events', component: Events },
  { path: '/projects', component: Projects },
  { path: '/chat', component: Chat },
  { path: '/contact', component: Contact }
]

export default new VueRouter({
  mode: 'history',
  routes
})
